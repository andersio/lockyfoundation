# Basic Usage
## Add this framework to your project
1. Download the source code, and add the `LockyFoundation.xcodeproj` to your project.
2. Go to your project's configuration (the top-level entity in your sidebar).
3. Click the `+` button in "Embedded Binaries", and choose `LockyFoundation.framework`.
4. Click the `+` button in "Linked Frameworks and Binaries", type `CoreBluetooth` and add the Core Bluetooth framework.
5. Done.

## Create a Lock Manager
```
import LockyFoundation
let lockManager = LockManager()
```

P.S. You must import the framework at the beginning of the file that uses the LockManager.

## Get a list of lock
`lockManager.locks` gives a list of lock which you can use to populate a `UITableView`.

## Get changes to the lock list
You may subscribe to the changes through the event producer (`lockManager.locks.eventProducer`), but it is recommended to use `TableViewAdapter` from MantleData instead.

## Authenticate a particular lock
You may initiate an authentication request upon selection using `lockManager.authenticateLock(lock:callback:)`. The callback would be called with a `AuthenticationResult` enum value, indicating whether the authentication is successful or not, or if the connection is failed. You may want to prompt the users about all the possible results.

# License
Copyright 2015 Anders Ha.
Licensed under Apache License v2.0.