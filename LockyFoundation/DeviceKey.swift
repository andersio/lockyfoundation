//
//  DeviceKey.swift
//  LockyFoundation
//
//  Created by Anders on 8/4/2016.
//  Copyright © 2016 Anders. All rights reserved.
//

import Foundation
import Security

public class DeviceKeyPair {
  public let prefix: String

  private var publicKey: SecKey
  private var privateKey: SecKey

  public lazy var blockSize: Int = { [unowned self] in
    let size1 = SecKeyGetBlockSize(self.publicKey)
    let size2 = SecKeyGetBlockSize(self.privateKey)

    assert(size1 == size2, "DeviceKeyPair: mismatched block size")
    return size1
    }()

  public var publicKeyString: String {
    if let data = DeviceKeyPair.publicKeyData(prefix: prefix) {
      return DeviceKeyPair.paddedStringForData(data)
    } else {
      return ""
    }
  }

  public var publicKeyData: NSData {
    return DeviceKeyPair.publicKeyData(prefix: prefix) ?? NSData()
  }

  public var publicKeyBase64ISOLatin1Data: NSData {
    return DeviceKeyPair.publicKeyBase64ISOLatin1Data(prefix: prefix) ?? NSData()
  }

  public init(prefix: String = "DeviceKeyPair") {
    self.prefix = prefix

    if let pair = DeviceKeyPair.getKey(prefix: prefix) {
      self.publicKey = pair.0
      self.privateKey = pair.1
    } else {
      let (key1, key2) = DeviceKeyPair.createKey(prefix: prefix)
      self.publicKey = key1
      self.privateKey = key2
    }
  }

  func decrypt(data: NSData) -> NSData? {
    let uint8Pointer = UnsafePointer<UInt8>(data.bytes)

    let realBlockSize = SecKeyGetBlockSize(privateKey)
    let blockSize = realBlockSize - 11

    let numberOfBlocks = Int(ceil(Double(data.length) / Double(realBlockSize)))
    var remainingData = data.length

    var bufferHead = UnsafeMutablePointer<UInt8>.alloc(blockSize * numberOfBlocks)
    var bufferIterator = bufferHead
    var uint8Iterator = uint8Pointer

    var aggregatedLength = 0

    for iteration in 0 ..< numberOfBlocks {
      var length = blockSize

      let resultCode = SecKeyDecrypt(privateKey,
                                     .PKCS1,
                                     uint8Iterator,
                                     remainingData >= realBlockSize ? realBlockSize : remainingData,
                                     bufferIterator,
                                     &length)

      guard resultCode == noErr else {
        return nil
      }

      remainingData -= realBlockSize
      aggregatedLength += length
      uint8Iterator = uint8Iterator.advancedBy(realBlockSize)
      bufferIterator = bufferIterator.advancedBy(blockSize)
    }

    return NSData(bytes: bufferHead, length: aggregatedLength)
  }

  // hash the string with SHA-256, and sign the hash.

  public func signature(for data: NSData) -> NSData {
    let data = data.sha256
    let uint8Pointer = UnsafePointer<UInt8>(data.bytes)

    let blockSize = SecKeyGetBlockSize(privateKey)

    if blockSize - 11 < data.length {
      preconditionFailure("too large \(blockSize)")
    }

    var encryptedBuffer = UnsafeMutablePointer<UInt8>.alloc(blockSize)
    var length = blockSize

    let resultCode = SecKeyRawSign(privateKey, .PKCS1, uint8Pointer, data.length, encryptedBuffer, &length)

    guard resultCode == noErr else {
      preconditionFailure("failed to sign the data.")
    }

    return NSData(bytes: encryptedBuffer, length: length)
  }

  public func test(string: String) -> String {
    let sig = signature(for: string.dataUsingEncoding(NSUTF8StringEncoding)!)

    let remoteKey = RemoteKey(key: publicKey)
    let encryptedData = remoteKey.encrypt(string.dataUsingEncoding(NSUTF8StringEncoding)!)
    let decryptedData = decrypt(encryptedData)!
    guard let decryptedString = String(data: decryptedData, encoding: NSUTF8StringEncoding) else {
      preconditionFailure("failed to extract content")
    }

    if !remoteKey.verify(signature: sig, for: decryptedData) {
      preconditionFailure("failed test case 1")
    }

    if decryptedString != string {
      preconditionFailure("failed test case 2")
    }

    return decryptedString
  }
}

extension DeviceKeyPair {
  static private func createKey(prefix prefix: String) -> (SecKey, SecKey) {
    let prefixedKey1 = "\(prefix)_pub"
    let prefixedKey2 = "\(prefix)_pri"

    var ptr1: SecKey? = nil
    var ptr2: SecKey? = nil


    let status = SecKeyGeneratePair([
      kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
      kSecAttrKeySizeInBits as String: NSNumber(integer: 2048),
      kSecPublicKeyAttrs as String: [
        kSecAttrIsPermanent as String: kCFBooleanTrue,
        kSecAttrApplicationTag as String: prefixedKey1.dataUsingEncoding(NSUTF8StringEncoding)!] as [String: NSObject],
      kSecPrivateKeyAttrs as String: [
        kSecAttrIsPermanent as String: kCFBooleanTrue,
        kSecAttrApplicationTag as String: prefixedKey2.dataUsingEncoding(NSUTF8StringEncoding)!]] as [String: NSObject]
      as NSDictionary as CFDictionary,
                                    &ptr1,
                                    &ptr2)

    guard status == noErr else {
      preconditionFailure("failed to save public key")
    }

    return (ptr1!, ptr2!)
  }

  static private func publicKeyData(prefix prefix: String) -> NSData? {
    let prefixedKey = "\(prefix)_pub"

    var query: [String: NSObject] = [
      KeychainSwiftConstants.klass       : kSecClassKey,
      kSecAttrApplicationTag as String : prefixedKey.dataUsingEncoding(NSUTF8StringEncoding)!,
      kSecReturnData as String : kCFBooleanTrue,
      KeychainSwiftConstants.matchLimit  : kSecMatchLimitOne ]

    var result: AnyObject?

    let resultCode = withUnsafeMutablePointer(&result) {
      SecItemCopyMatching(query, UnsafeMutablePointer($0))
    }

    if resultCode == noErr {
      return result as! NSData
    }

    return nil
  }

  static private func publicKeyBase64ISOLatin1Data(prefix prefix: String) -> NSData? {
    if let data = publicKeyData(prefix: prefix) {
      return data.base64EncodedStringWithOptions([]).dataUsingEncoding(NSISOLatin1StringEncoding)!
    }
    
    return nil
  }

  static public func paddedStringForData(data: NSData) -> String {
      let bytes = UnsafeMutablePointer<CUnsignedChar>(data.bytes)
      var aggregatedChars = ""
      var pendingChars = ""

      for i in 0 ..< data.length {
        let numbers = String(format:"%X", bytes[i])
        let appendingEnd = numbers.startIndex.advancedBy(4 - pendingChars.characters.count, limit: numbers.endIndex)
        pendingChars.appendContentsOf(numbers.substringWithRange(numbers.startIndex ..< appendingEnd))

        if pendingChars.characters.count == 4 && appendingEnd < numbers.endIndex {
          aggregatedChars = "\(aggregatedChars) \(pendingChars)"
          pendingChars = "\(numbers.substringWithRange(appendingEnd ..< numbers.endIndex))"
        }
      }

      if pendingChars.characters.count > 0 {
        aggregatedChars = "\(aggregatedChars) \(pendingChars)"
      }
      return aggregatedChars[aggregatedChars.startIndex.advancedBy(1, limit: aggregatedChars.endIndex) ..< aggregatedChars.endIndex]
  }

  static private func getKey(prefix prefix: String) -> (SecKey, SecKey)? {
    func handler(for type: String) -> SecKey? {
      let prefixedKey = "\(prefix)_\(type)"

      var query: [String: NSObject] = [
        KeychainSwiftConstants.klass       : kSecClassKey,
        kSecAttrApplicationTag as String : prefixedKey.dataUsingEncoding(NSUTF8StringEncoding)!,
        kSecReturnRef as String : kCFBooleanTrue,
        KeychainSwiftConstants.matchLimit  : kSecMatchLimitOne ]
      
      var result: AnyObject?
      
      let resultCode = withUnsafeMutablePointer(&result) {
        SecItemCopyMatching(query, UnsafeMutablePointer($0))
      }
      
      if resultCode == noErr { return result as! SecKey }
      
      return nil
    }
    
    if let pub = handler(for: "pub"), pri = handler(for: "pri") {
      return (pub, pri)
    }
    
    return nil
  }
}
