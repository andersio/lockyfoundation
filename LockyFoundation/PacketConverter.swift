//
//  PacketConverter.swift
//  LockyFoundation
//
//  Created by Anders on 11/4/2016.
//  Copyright © 2016 Anders. All rights reserved.
//

class PacketConverter {
  static func requestPacket(deviceKeyPair keyPair: DeviceKeyPair, data encryptedData: NSData) -> Packet? {
    return keyPair.decrypt(encryptedData).flatMap { data in
      guard data.length >= 2 else {
        print("missing header.")
        return nil
      }

      let rawBytes = UnsafePointer<UInt8>(data.bytes)

      // 2-byte response header
      guard rawBytes[0] == 32 && rawBytes[1] == 74 else {
        print("invalid packet header")
        return nil
      }

      let remoteBlockSize = Int(UnsafePointer<UInt16>(rawBytes.advancedBy(2)).memory)
      let expectedHeaderLength = 2 + 2 + remoteBlockSize

      guard data.length >= expectedHeaderLength else {
        print("invalid packet format")
        return nil
      }

      // client public key used to sign signature later on.
      let clientPublicKey = data.subdataWithRange(NSRange(location: 4, length: remoteBlockSize))
      let data = data.subdataWithRange(NSRange(location: expectedHeaderLength, length: data.length - expectedHeaderLength))

      guard let json = try? NSJSONSerialization.JSONObjectWithData(data, options: []) else {
        print("invalid JSON format.")
        return nil
      }

      return (try? RemoteKey(rawKey: clientPublicKey)).flatMap {
        Packet(clientPublicKey: $0,
          dictionary: json as! [String: NSObject])
      }
    }
  }

  static func responsePacket(deviceKeyPair keyPair: DeviceKeyPair, remotePublicKey: RemoteKey, data encryptedData: NSData) -> Packet? {
    print("response byte: \(encryptedData.length)")
    return keyPair.decrypt(encryptedData).flatMap { data in
      guard data.length >= 2 else {
        print("missing header.")
        return nil
      }

      let rawBytes = UnsafePointer<UInt8>(data.bytes)

      // 2-byte response header
      guard rawBytes[0] == 32 && rawBytes[1] == 75 else {
        print("invalid packet header")
        return nil
      }

      let remoteBlockSize = Int(UnsafePointer<UInt16>(rawBytes.advancedBy(2)).memory)
      let expectedHeaderLength = 2 + 2 + remoteBlockSize

      guard data.length >= expectedHeaderLength else {
        print("invalid packet format")
        return nil
      }

      // client public key used to sign signature later on.
      let signature = data.subdataWithRange(NSRange(location: 4, length: remoteBlockSize))
      let data = data.subdataWithRange(NSRange(location: expectedHeaderLength, length: data.length - expectedHeaderLength))

      guard remotePublicKey.verify(signature: signature, for: data.sha256) else {
        print("signature verification failed.")
        return nil
      }

      guard let json = try? NSJSONSerialization.JSONObjectWithData(data, options: []) else {
        print("invalid JSON format.")
        return nil
      }

      return Packet(clientPublicKey: remotePublicKey,
        dictionary: json as! [String: NSObject])
    }
  }

  static func dataFor(request packet: Packet, deviceKeyPair: DeviceKeyPair) -> NSData? {
    let rawBytes = UnsafeMutablePointer<UInt8>.alloc(4)
    rawBytes[0] = 32
    rawBytes[1] = 74

    let publicKeyData = deviceKeyPair.publicKeyData
    UnsafeMutablePointer<UInt16>(rawBytes.advancedBy(2)).memory = UInt16(publicKeyData.length)

    let pushData = NSMutableData(bytesNoCopy: rawBytes, length: 4, deallocator: { _ in
      rawBytes.destroy()
    })
    pushData.appendData(publicKeyData)

    // Data is encoded in UTF-8 by NSJSONSerialization.
    guard let JSONData = try? NSJSONSerialization.dataWithJSONObject(packet.dictionary, options: []) else {
      return nil
    }

    pushData.appendData(JSONData)

    return packet.clientPublicKey.encrypt(pushData)
  }

  static func dataFor(response packet: Packet, deviceKeyPair: DeviceKeyPair) -> NSData? {
    guard let data = try? NSJSONSerialization.dataWithJSONObject(packet.dictionary, options: []) else {
      return nil
    }

    // hash & sign the data
    let signature = deviceKeyPair.signature(for: data.sha256)
    let pushData = NSMutableData(length: 4)!

    let rawBytes = UnsafeMutablePointer<UInt8>(pushData.mutableBytes)
    rawBytes[0] = 32
    rawBytes[1] = 75
    UnsafeMutablePointer<UInt16>(rawBytes.advancedBy(2)).memory = UInt16(signature.length)

    pushData.appendData(signature)
    pushData.appendData(data)
    
    return packet.clientPublicKey.encrypt(pushData)
  }
}