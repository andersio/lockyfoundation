//
//  LockController.swift
//  LockyFoundation
//
//  Created by Anders on 18/3/2016.
//  Copyright © 2016 Anders. All rights reserved.
//

import Foundation
import CoreBluetooth
import MantleData
import ReactiveCocoa

//
//  LockManager.swift
//  LockyFoundation
//
//  Copyright © 2015 Anders Ha. All rights reserved.
//  Licensed under Apache License v2.0.
//

public class LockController: NSObject {
  static let serviceUUID = CBUUID(string: "FEE0")

  private var poweredOn: Bool = false
  private var scanning: Bool = false

  public let lock = MutableProperty<CBPeripheral?>(nil)

  private(set) public var centralManager: CBCentralManager!

  public override init() {
    super.init()

    centralManager = CBCentralManager(delegate: self,
                                      queue: dispatch_get_main_queue())

    lock.producer.startWithNext {
      guard let lock = $0 else {
        return
      }

      print("LOCKCTL: discovered a BLE LOCK controller [\(lock.identifier)]")
    }
  }

  public func connect() {
    if let peripheral = lock.value {
      centralManager.connectPeripheral(peripheral, options: nil)
    } else {
      print("LOCKCTL: attempt to disconnect without discovered lock controller")
    }
  }

  public func disconnect() {
    if let peripheral = lock.value {
      centralManager.cancelPeripheralConnection(peripheral)
    } else {
      print("LOCKCTL: attempt to disconnect without connected lock controller")
    }
  }
}

extension LockController: CBCentralManagerDelegate {
  public func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
    guard let services = advertisementData[CBAdvertisementDataServiceUUIDsKey] as? [CBUUID] else {
      return
    }

    if services.contains(LockController.serviceUUID) {
      if let currentLock = lock.value {
        if currentLock.identifier == peripheral.identifier {
          return
        } else {
          centralManager.cancelPeripheralConnection(currentLock)
        }
      }

      lock.value = peripheral
    }
  }

  public func centralManagerDidUpdateState(central: CBCentralManager) {
    if case .PoweredOn = central.state {
      poweredOn = true
      scanning = true
      central.scanForPeripheralsWithServices([LockController.serviceUUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
    }
  }

  public func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
    guard let lock = lock.value where lock.identifier == peripheral.identifier else {
      return
    }
    print("LOCKCTL: connected to lock [\(lock.identifier)]")
  }

  public func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
    guard let lock = lock.value where lock.identifier == peripheral.identifier else {
      return
    }

    print("LOCKCTL: disconnected the lock [\(lock.identifier)]")
  }

  public func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
    NSLog("lockCTL: failed to connect lock [\(peripheral.identifier)]")
  }
}