//
//  LockRequest.swift
//  LockyFoundation
//
//  Copyright © 2015 Anders Ha. All rights reserved.
//  Licensed under Apache License v2.0.
//

import Foundation

final public class LockRequest: NSOperation {
  public enum Response {
    case Success([String: AnyObject])
    case Aborted
  }

  public var semaphore = Semaphore()
  public var responseHandler: (Response -> Void)?
  public let responseKey: String
  public let key: String
  public let data: [String: NSObject]

  internal var lockObject: Lock!

  private var operationExecuting = false
  private var operationFinished = false
  private var observerRemoved = false

  private var isSent = false

  public override var executing: Bool {
    get {
      return operationExecuting
    }
    set {
      if operationExecuting != newValue {
        willChangeValueForKey("isExecuting")
        operationExecuting = newValue
        didChangeValueForKey("isExecuting")
      }
    }
  }
  public override var finished: Bool {
    get {
      return operationFinished
    }
    set {
      if operationFinished != newValue {
        willChangeValueForKey("isFinished")
        operationFinished = newValue
        didChangeValueForKey("isFinished")
      }
    }
  }

  public init(key: String, data: [String: NSObject], responseKey: String, responseHandler: Response -> Void) {
    self.key              = key
    self.data             = data
    self.responseKey      = responseKey
    self.responseHandler  = responseHandler

    super.init()
    addObserver(self, forKeyPath: "isCancelled", options: [.Old, .New], context: nil)
  }

  public override func main() {
    preconditionFailure("lockreq: unanticipated call to LockRequest.main().")
  }

  public override func start() {
    executing = true
    print("lock: op dispatched")

    if cancelled {
      executing = false
      finished = true
      return
    }

    guard let lockObject = lockObject else {
      preconditionFailure("lockreq: framework failed to inject a Lock reference.")
    }

    lockObject.currentRequest = self

    let requestPacket = Packet(clientPublicKey: lockObject.key!,
                               dictionary: [key: data])
    guard let rawData = PacketConverter.dataFor(request: requestPacket, deviceKeyPair: lockObject.lockManager.deviceKey) else {
      print("lockreq: failed to construct an encrypted request packet. operation aborted.")
      executing = false
      finished = true
      return
    }

    lockObject.peripheralObject.writeValue(rawData,
                                           forCharacteristic: lockObject.requestChannel,
                                           type: .WithResponse)

    NSLog("lockreq: posted data to request channel of length \(rawData.length)")
    isSent = true
  }

  public func processResponseChannel(data: NSData) {
    let response: Response

    defer {
      pushResponse(response)
      lockObject.currentRequest = nil
      executing = false
      finished = true
    }

    if finished {
      preconditionFailure("lockreq: unanticipated call at finished state")
    }

    if executing && cancelled {
      response = .Aborted
      executing = false
      finished = true
      return
    }

    guard let packet = PacketConverter.responsePacket(deviceKeyPair: lockObject.lockManager.deviceKey, remotePublicKey: lockObject.key!, data: data) else {
      print("lockreq: cannot decrypt the response. aborted the operation.")
      response = .Aborted
      return
    }

    guard let responseData = packet.dictionary[responseKey] as? [String: NSObject] else {
      print("lockreq: content of packet does not match supplied parameters. aborted the operation.")
      response = .Aborted
      return
    }

    print("response data: \(responseData.count) entries")
    response = .Success(responseData)
  }

  internal func pushResponse(response: Response) {
    if responseHandler != nil {
      self.semaphore.acquire()
      responseHandler!(response)
      responseHandler = nil
      self.semaphore.release()
    }
  }

  public override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
    if let keyPath = keyPath, object = object where keyPath == "isCancelled" && object === self {
      let old = change?[NSKeyValueChangeOldKey] as? Bool ?? false
      let new = change?[NSKeyValueChangeNewKey] as? Bool ?? false
      if old != new && new && isSent {
        removeObserver(self, forKeyPath: "isCancelled", context: nil)
        observerRemoved = true
        lockObject.currentRequest = nil
        pushResponse(.Aborted)
        executing = false
        finished = true
      }
    } else {
      super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
    }
  }
  
  deinit {
    if !observerRemoved {
      removeObserver(self, forKeyPath: "isCancelled", context: nil)
    }
    print("op dequeued")
  }
}