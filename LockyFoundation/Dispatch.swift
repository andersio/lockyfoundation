//
//  Dispatch.swift
//  LockyFoundation
//
//  Copyright © 2015 Anders Ha. All rights reserved.
//  Licensed under Apache License v2.0.
//

import Foundation

public var MainQueue: dispatch_queue_t = dispatch_get_main_queue()

public struct Semaphore {
  private let semaphore = dispatch_semaphore_create(1)
  public var enable = true

  public func acquire(time: dispatch_time_t = DISPATCH_TIME_FOREVER) {
    if enable {
      dispatch_semaphore_wait(semaphore, time)
    }
  }

  public func release() {
    if enable {
      dispatch_semaphore_signal(semaphore)
    }
  }
}

extension dispatch_queue_t {
  public func async(closure: Void -> Void) {
    dispatch_async(self, closure)
  }

  public func sync(closure: Void -> Void) {
    dispatch_sync(self, closure)
  }

  public func after(time: Double, closure: Void -> Void) {
    let time = dispatch_time(DISPATCH_TIME_NOW, Int64(time * Double(NSEC_PER_SEC)))
    dispatch_after(time, self, closure)
  }
}