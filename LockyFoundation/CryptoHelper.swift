//
//  CryptoHelper.swift
//  LockyFoundation
//
//  Created by Anders on 8/4/2016.
//  Copyright © 2016 Anders. All rights reserved.
//

import CommonCrypto

extension NSData {
  public var sha256: NSData {
    let ret = NSMutableData(length: Int(CC_SHA256_DIGEST_LENGTH))!
    CC_SHA256(bytes, CC_LONG(length), UnsafeMutablePointer(ret.mutableBytes))
    return ret
  }
}