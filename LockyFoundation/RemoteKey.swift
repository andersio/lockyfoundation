//
//  RemoteKey.swift
//  LockyFoundation
//
//  Created by Anders on 8/4/2016.
//  Copyright © 2016 Anders. All rights reserved.
//

import Security

class RemoteKey {
  enum Error: ErrorType {
    case CannotConstructSecKeyObject
  }

  private let key: SecKey
  public let blockSize: Int

  init(rawKey: NSData) throws {
    var result: AnyObject?

    SecItemDelete([
      kSecClass as String: kSecClassKey,
      kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
      kSecAttrApplicationTag as String: "_TMP".dataUsingEncoding(NSUTF8StringEncoding)!,
      kSecAttrIsPermanent as String: kCFBooleanFalse
      ])

    let resultCode = SecItemAdd([
      kSecClass as String: kSecClassKey,
      kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
      kSecValueData as String: rawKey,
      kSecAttrApplicationTag as String: "_TMP".dataUsingEncoding(NSUTF8StringEncoding)!,
      kSecReturnRef as String: kCFBooleanTrue,
      kSecAttrIsPermanent as String: kCFBooleanFalse
      ], &result)

    guard resultCode == noErr else {
      throw Error.CannotConstructSecKeyObject
    }

    key = result as! SecKey
    blockSize = SecKeyGetBlockSize(key)
  }

  init(key: SecKey) {
    self.key = key
    self.blockSize = SecKeyGetBlockSize(key)
  }

  func encrypt(data: NSData) -> NSData {
    let uint8Pointer = UnsafePointer<UInt8>(data.bytes)

    let realBlockSize = SecKeyGetBlockSize(key)
    let blockSize = realBlockSize - 11

    let numberOfBlocks = Int(ceil(Double(data.length) / Double(blockSize)))
    var remainingData = data.length

    var encryptedBufferHead = UnsafeMutablePointer<UInt8>.alloc(realBlockSize * numberOfBlocks)

    var aggregatedLength = 0

    for iteration in 0 ..< numberOfBlocks {
      var length = realBlockSize
      var dataLength = remainingData >= blockSize ? blockSize : remainingData
      let resultCode = SecKeyEncrypt(key,
                                     .PKCS1,
                                     uint8Pointer.advancedBy(blockSize * iteration),
                                     dataLength,
                                     encryptedBufferHead.advancedBy(realBlockSize * iteration),
                                     &length)

      guard resultCode == noErr else {
        preconditionFailure("failed to sign the data.")
      }

      remainingData -= blockSize
      aggregatedLength += length
    }

    return NSData(bytes: encryptedBufferHead, length: aggregatedLength)
  }


  func verify(signature signature: NSData, for data: NSData) -> Bool {
    let data = data.sha256
    let blockSize = SecKeyGetBlockSize(key)

    if blockSize < signature.length {
      preconditionFailure("too large \(blockSize)")
    }

    var decryptedData = UnsafeMutablePointer<UInt8>.alloc(blockSize)
    var length = blockSize
    
    let resultCode = SecKeyRawVerify(key,
                                     .PKCS1,
                                     UnsafePointer<UInt8>(data.bytes),
                                     data.length,
                                     UnsafePointer<UInt8>(signature.bytes),
                                     signature.length)
    
    return resultCode == noErr
  }
}
