//
//  Extension.Collection.swift
//  LockyFoundation
//
//  Copyright © 2015 Anders Ha. All rights reserved.
//  Licensed under Apache License v2.0.
//

import CoreBluetooth

extension SequenceType where Generator.Element == Lock {
  public func contains(UUID UUID: NSUUID) -> Bool {
    for element in self {
      if element.peripheralObject.identifier.isEqual(UUID) {
        return true
      }
    }

    return false
  }
}

extension CollectionType where Generator.Element == Lock {
  public func indexOf(UUID UUID: NSUUID) -> Index? {
    return indexOf { element in
      return element.peripheralObject.identifier.isEqual(UUID)
    }
  }
}

extension SequenceType where Generator.Element: CBAttribute {
  public func contains(UUID UUID: CBUUID) -> Bool {
    for element in self {
      if element.UUID.isEqual(UUID) {
        return true
      }
    }

    return false
  }
}

extension CollectionType where Generator.Element: CBAttribute, Index == Int {
  public func indexOf(UUID UUID: CBUUID) -> Index? {
    for (i, element) in self.enumerate() {
      if element.UUID.isEqual(UUID) {
        return i
      }
    }

    return nil
  }
}

extension CollectionType where Generator.Element: CBPeer, Index == Int {
  public func indexOf(UUID UUID: NSUUID) -> Index? {
    for (i, element) in self.enumerate() {
      if element.identifier.isEqual(UUID) {
        return i
      }
    }

    return nil
  }
}

extension SequenceType where Generator.Element: CBPeer {
  public func contains(UUID UUID: NSUUID) -> Bool {
    for element in self {
      if element.identifier.isEqual(UUID) {
        return true
      }
    }

    return false
  }
}