//
//  LockyFoundation.h
//  LockyFoundation
//
//  Copyright © 2015 Anders Ha. All rights reserved.
//  Licensed under Apache License v2.0.
//

#import <UIKit/UIKit.h>

//! Project version number for LockyFoundation.
FOUNDATION_EXPORT double LockyFoundationVersionNumber;

//! Project version string for LockyFoundation.
FOUNDATION_EXPORT const unsigned char LockyFoundationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LockyFoundation/PublicHeader.h>


