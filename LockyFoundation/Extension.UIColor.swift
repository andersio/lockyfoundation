//
//  Extension.UIColor.swift
//  LockyFoundation
//
//  Copyright © 2015 Anders Ha. All rights reserved.
//  Licensed under Apache License v2.0.
//

import Foundation
import UIKit

extension UIColor {
  public convenience init(hexCode hex: Int32) {
    let b = CGFloat((hex & 0x000000FF)) / 255.0
    let g = CGFloat((hex & 0x0000FF00) >> 8) / 255.0
    let r = CGFloat((hex & 0x00FF0000) >> 16) / 255.0

    self.init(red: r, green: g, blue: b, alpha: 1.0)
  }
}