//
//  Lock.swift
//  LockyFoundation
//
//  Copyright © 2015 Anders Ha. All rights reserved.
//  Licensed under Apache License v2.0.
//

import Foundation
import CoreBluetooth
import MantleData
import ReactiveCocoa

final public class Lock: NSObject {
  static internal let ServiceUUID = CBUUID(string: "589E0BA8-CAEE-40A7-9CEC-A58128F52584")
  static internal let responseUUID = CBUUID(string: "39FF25F8-307F-4BA1-A009-8351B10DEB3A")
  static internal let requestUUID = CBUUID(string: "807E8387-D2D0-414C-8EAA-D61092E3F66D")
  static internal let notificationUUID = CBUUID(string: "AE6EC3D3-1EDA-4965-AD75-F94E9BDE7F7D")
  static internal let publicKeyUUID = CBUUID(string: "f7e7b8aa-9d33-4655-9c97-87a55d7f581b")

  internal let peripheralObject: CBPeripheral
  internal var service: CBService!
  internal var requestChannel: CBCharacteristic!

  public var UUID: NSUUID       { return peripheralObject.identifier }
  public var UUIDString: String { return peripheralObject.identifier.UUIDString }

  internal weak var lockManager: LockManager!
  internal var key: RemoteKey?

  public let name = MutableProperty<String>("(locky-nil)")
  public let proximity = MutableProperty<Double>(-1)
  public let outOfRange = MutableProperty<Bool>(true)
  public internal(set) var persisted: Bool = false

  internal var backgroundNotified = false
  internal var backgroundTimer: CFTimeInterval?
  internal var updateTimer: CFTimeInterval?
  internal var blockNotificationUntilTime: CFTimeInterval?
  internal var hasPendingAction: Bool = false
  public private(set) var latestDoorbellImage: UIImage?

  private var notificationBuffer: [NSUUID: NSMutableData] = [:]
  private var responseBuffer: [NSUUID: NSMutableData] = [:]

  internal weak var currentRequest: LockRequest?
  internal var processing: Bool { return queue.operationCount > 0 }
  private var discoveredChannels = 0

  internal lazy var queue: NSOperationQueue = {
    let queue = NSOperationQueue()
    queue.maxConcurrentOperationCount = 1
    queue.suspended = true
    return queue
  }()

  public var isConnected: Bool { return peripheralObject.state == .Connected }

  public var keychainItem: String? {
    get {
      return lockManager.keychain.get(UUIDString)
    }
    set {
      if let passcode = newValue {
        lockManager.keychain.set(passcode, forKey: UUIDString, withAccess: .AccessibleAfterFirstUnlock)
      } else {
        lockManager.keychain.delete(UUIDString)
      }
    }
  }

  internal init(lockManager manager: LockManager, peripheral: CBPeripheral, persisted lockPersisted: Bool = false, time: CFTimeInterval? = nil) {
    lockManager = manager
    peripheralObject = peripheral
    updateTimer = time
    persisted = lockPersisted
  }

  internal func connect() {
    queue.suspended = true
    lockManager.centralManager.connectPeripheral(peripheralObject, options: nil)
  }

  internal func initiateRequest(request: LockRequest) {
    print("lock: isOutOfRange: \(outOfRange.value), isConnected: \(isConnected)")
    if !isConnected {
      connect()
    }

    request.lockObject = self
    queue.addOperation(request)
    print("lock: op enqueued. queueCount: \(queue.operationCount)")

    MainQueue.after(3.0) {
      if !request.finished {
        request.cancel()
        print("lock: op timeout, cancellation requested. queueCount: \(self.queue.operationCount). queue suspended? \(self.queue.suspended)")
      }
    }
  }

  internal func didConnectPeripheral() {
    NSLog("authop: connected")
    peripheralObject.delegate = self
    peripheralObject.discoverServices([Lock.ServiceUUID])
  }

  internal func didDisconnectPeripheral(error: NSError?) {
    NSLog("authop: disconnected")
    currentRequest?.cancel()
    queue.cancelAllOperations()
    queue.suspended = true
  }

  internal func didFailToConnectPeripheral(error: NSError?) {
    NSLog("authop: failed")
    currentRequest?.cancel()
    queue.cancelAllOperations()
    queue.suspended = true
  }

  internal func checkReadiness() {
    if discoveredChannels == 4 {
      queue.suspended = false
      print("key: device is ready")
    } else {
      print("discovered channels: \(discoveredChannels)")
    }
  }

  internal func didReceivePublicKey(characteristic characteristic: CBCharacteristic, error: NSError?) {
    if let error = error {
      NSLog("lock: failed to retrieve public key (1). \(error.description)")
      return
    }

    guard let data = characteristic.value else {
      NSLog("lock: failed to retrieve public key (2).")
      return
    }

    print("key: lock: didReceivePublicKey - data found")

    do {
      key = try RemoteKey(rawKey: data)
    } catch let e {
      switch e {
      case RemoteKey.Error.CannotConstructSecKeyObject:
        print("key: lock: cannot construct key")
        return
      default:
        preconditionFailure("lock: didReceivePublicKey - unknown error.")
      }
    }

    print("key: lock: didReceivePublicKey - received")

    discoveredChannels += 1
    checkReadiness()
  }

  internal func didReceiveUpdateFromNotificationChannel(characteristic: CBCharacteristic, error: NSError?) {
    if let error = error {
      NSLog("lock: Response channel GATT error. \(error.description)")
      return
    }

    guard let packet = characteristic.value else {
      NSLog("lock: Failed to get value from notification characteristic.")
      return
    }

    let peripheralId = characteristic.service.peripheral.identifier

    if !notificationBuffer.keys.contains(peripheralId) {
      notificationBuffer[peripheralId] = NSMutableData()
    }

    if packet.length > 0 {
      notificationBuffer[peripheralId]!.appendData(packet)
    } else {
      defer {
        notificationBuffer.removeValueForKey(peripheralId)
      }

      guard let notificaitonPacket = PacketConverter.responsePacket(deviceKeyPair: lockManager.deviceKey,
                                                                    remotePublicKey: key!,
                                                                    data: notificationBuffer[peripheralId]!) else {
                                                                      return
      }

      guard let notification = notificaitonPacket.dictionary["notification"] as? [String: AnyObject],
        notifyFor = notification["for"] as? String
        else {
          NSLog("lock: Failed to unpack the notification packet.")
          return
      }

      switch (notifyFor) {
      case "doorbell":
        guard let rawImageString = notification["data"] as? String,
          rawImageData = NSData(base64EncodedString: rawImageString, options: []),
          image = UIImage(data: rawImageData)
          else {
            NSLog("lock: invalid doorbell notification. failed to extract.")
            return
        }

        latestDoorbellImage = image
        lockManager.processDoorbellNotification(self, image: image)
        break
      default:
        NSLog("lock: invalid notification type.")
        break
      }
    }
  }

  internal func didReceiveUpdateFromResponseChannel(characteristic: CBCharacteristic, error: NSError?) {
    if let error = error {
      NSLog("lock: Response channel GATT error. \(error.description)")
      return
    }

    guard let rawData = characteristic.value else {
      return
    }

    let peripheralId = characteristic.service.peripheral.identifier

    if !responseBuffer.keys.contains(peripheralId) {
      responseBuffer[peripheralId] = NSMutableData()
    }

    if rawData.length > 0 {
      responseBuffer[peripheralId]!.appendData(rawData)
    } else {
      defer {
        responseBuffer.removeValueForKey(peripheralId)
      }

      currentRequest?.processResponseChannel(responseBuffer[peripheralId]!)
    }
  }

  override public func isEqual(object: AnyObject?) -> Bool {
    switch (object){
    case let object as Lock:
      return peripheralObject.identifier.isEqual(object.peripheralObject.identifier)
    default:
      preconditionFailure("lock: equality against non-Lock or non-CBPeer instance.")
    }
  }

  deinit {
    NSLog("lock: dealloc")
  }
}

extension Lock: CBPeripheralDelegate {
  public func peripheral(peripheral: CBPeripheral, didDiscoverServices error: NSError?) {
    if let error = error {
      preconditionFailure(error.description)
    }

    NSLog("lock: walking through discovered service")
    discoveredChannels = 0

    if let index = peripheral.services?.indexOf(UUID: Lock.ServiceUUID) {
      let service = peripheral.services![index]
      self.service = service

      if service.characteristics == nil || service.characteristics!.count < 4 {
        peripheral.discoverCharacteristics([Lock.responseUUID, Lock.requestUUID, Lock.notificationUUID, Lock.publicKeyUUID], forService: service)
      }
    }
  }

  public func peripheral(peripheral: CBPeripheral, didDiscoverCharacteristicsForService service: CBService, error: NSError?) {
    if let error = error {
      preconditionFailure(error.description)
    }

    guard let characteristics = service.characteristics else {
      return
    }

    if let index = characteristics.indexOf(UUID: Lock.responseUUID) {
      let response = service.characteristics![index]
      if !response.isNotifying {
        peripheral.setNotifyValue(true, forCharacteristic: response)
      }
      discoveredChannels += 1
    }

    if let index = characteristics.indexOf(UUID: Lock.notificationUUID) {
      let notificationChannel = service.characteristics![index]
      if !notificationChannel.isNotifying {
        peripheral.setNotifyValue(true, forCharacteristic: notificationChannel)
      }
      discoveredChannels += 1
    }

    if let index = characteristics.indexOf(UUID: Lock.requestUUID) {
      requestChannel = service.characteristics![index]
      discoveredChannels += 1
    }

    if let index = characteristics.indexOf(UUID: Lock.publicKeyUUID) {
      peripheral.readValueForCharacteristic(characteristics[index])
    }

    checkReadiness()
  }

  public func peripheral(peripheral: CBPeripheral, didWriteValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
    if let error = error {
      NSLog("lock: failed to write value. \(error.localizedDescription)")
      print("currentReq: \(currentRequest); queueCount: \(queue.operationCount)")
      currentRequest?.cancel()
      lockManager.centralManager.cancelPeripheralConnection(peripheral)
      return
    }

    NSLog("lock: write success")
  }

  public func peripheral(peripheral: CBPeripheral, didUpdateNotificationStateForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
    if let error = error {
      NSLog("lock: failed to subscribe \(characteristic.UUID == Lock.notificationUUID ? "notification channel" : "response channel"). \(error.description)")
      lockManager.centralManager.cancelPeripheralConnection(peripheral)
      return
    }

    NSLog("lock: \(characteristic.UUID == Lock.notificationUUID ? "notification channel" : "response channel") is established.")
  }

  public func peripheral(peripheral: CBPeripheral, didUpdateValueForCharacteristic characteristic: CBCharacteristic, error: NSError?) {
    switch(characteristic.UUID) {
    case Lock.notificationUUID:
      didReceiveUpdateFromNotificationChannel(characteristic, error: error)
      break
    case Lock.responseUUID:
      didReceiveUpdateFromResponseChannel(characteristic, error: error)
      break
    case Lock.publicKeyUUID:
      didReceivePublicKey(characteristic: characteristic, error: error)
      break
    default:
      preconditionFailure("lock: received update value from unknown subscription channel.")
    }
  }
}