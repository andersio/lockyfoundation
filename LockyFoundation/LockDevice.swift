//
//  LockDevice.swift
//  LockyFoundation
//
//  Copyright © 2015 Anders Ha. All rights reserved.
//  Licensed under Apache License v2.0.
//

import Foundation
import CoreBluetooth
import MantleData
import ReactiveCocoa

struct Packet {
  let clientPublicKey: RemoteKey
  let dictionary: [String: NSObject]
}

public class LockDevice: NSObject {
  internal enum Event {
    case Response(NSData)
    //case Notificaiton(NSData)
  }

  public enum AuthenticationResult {
    case Success(String?)
    case Failed
  }

  private var manager: CBPeripheralManager!
  private let service: CBMutableService
  private let responseChannel: CBMutableCharacteristic
  private let requestChannel: CBMutableCharacteristic
  private let notificationChannel: CBMutableCharacteristic
  private let publicKeyChannel: CBCharacteristic

  private let lockController: LockController
  public let deviceKeyPair = DeviceKeyPair()

  private let subscribers: ArraySetSection<CBCentral>
  private var subscriberPublicKey: [NSUUID: RemoteKey] = [:]
  private var authenticated = Set<NSUUID>()
  private var pendingEvents: [NSUUID: [Event]] = [:]

  private var powerOnCallback: (Void -> Void)?

  public private(set) var numberOfSubscribers = MutableProperty<Int>(0)
  public private(set) var numberOfTransactions = MutableProperty<Int>(0)
  public private(set) var authenticatedCount = MutableProperty<Int>(0)

  public private(set) var response = MutableProperty<AuthenticationResult?>(nil)
  public private(set) var advertising = MutableProperty<Bool>(false)
  public private(set) var notifying = MutableProperty<Bool>(false)
  public private(set) var isUnlocked = MutableProperty<Bool>(false)

  private let sendQueue = NSOperationQueue()
  private var sendingOp: SendOperation? = nil

  public init(powerOnCallback: (Void -> Void)? = nil) {
    self.powerOnCallback = powerOnCallback
    self.service = CBMutableService(type: Lock.ServiceUUID, primary: true)
    self.subscribers = ArraySetSection(name: ReactiveSetSectionName(nil), values: [])

    responseChannel = CBMutableCharacteristic(type: Lock.responseUUID,
                                              properties: [.Read, .Notify],
                                              value: nil,
                                              permissions: [.Readable])

    notificationChannel = CBMutableCharacteristic(type: Lock.notificationUUID,
                                                  properties: [.Read, .Notify],
                                                  value: nil,
                                                  permissions: [.Readable])

    publicKeyChannel = CBMutableCharacteristic(type: Lock.publicKeyUUID,
                                               properties: [.Read],
                                               value: deviceKeyPair.publicKeyData,
                                               permissions: [.Readable])

    print("raw public key bytes: \(deviceKeyPair.publicKeyData.length)")

    requestChannel = CBMutableCharacteristic(type: Lock.requestUUID,
                                             properties: [.Write],
                                             value: nil,
                                             permissions: [.Writeable])

    lockController = LockController()

    super.init()
    manager = CBPeripheralManager(delegate: self, queue: dispatch_get_main_queue(), options: nil)
  }

  private func setup() {
    service.characteristics = [requestChannel, responseChannel, notificationChannel, publicKeyChannel]
    manager.addService(service)
    powerOnCallback?()
  }

  public func isValidLocalName(name: String) -> Bool {
    if name.dataUsingEncoding(NSUTF8StringEncoding)!.length > 8 {
      return false
    }

    return true
  }

  public func startAdvertising(name: String) {
    if manager.isAdvertising {
      advertising.value = false
      manager.stopAdvertising()
    }

    NSLog("lock name: \(name)")

    if isValidLocalName(name) {
      manager.startAdvertising([CBAdvertisementDataServiceUUIDsKey: [Lock.ServiceUUID] as NSArray, CBAdvertisementDataLocalNameKey: name])
    }
  }

  public func notifyDoorbellPushed(image: UIImage, completionBlock: (Void -> Void)? = nil) {
    if manager.isAdvertising && subscribers.count > 0 {
      let centrals = subscribers.filter { authenticated.contains($0.identifier) && subscriberPublicKey[$0.identifier] != nil }
      if centrals.count > 0 {
        guard let rawJPEG = UIImageJPEGRepresentation(image, 0.4) else {
          return
        }

        let imageStr = rawJPEG.base64EncodedStringWithOptions([])
        notifying.producer.skipWhile({ $0 }).take(1).startWithNext { bool in
          completionBlock?()
        }

        centrals.forEach { central in
          let notificationPacket = Packet(clientPublicKey: subscriberPublicKey[central.identifier]!,
            dictionary: ["notification": ["for": "doorbell", "data": imageStr]])

          guard let rawData = PacketConverter.dataFor(response: notificationPacket, deviceKeyPair: deviceKeyPair) else {
            return
          }

          send(binaryData: rawData,
            characteristic: notificationChannel,
            centrals: [central])
        }

        return
      }
    }

    completionBlock?()
  }

  internal func unlock() {
    isUnlocked.value = true
    lockController.connect()
  }

  public func relock() {
    isUnlocked.value = false
    lockController.disconnect()
  }
}

extension LockDevice: CBPeripheralManagerDelegate {
  public func peripheralManagerDidUpdateState(peripheral: CBPeripheralManager) {
    if peripheral.state == .PoweredOn {
      NSLog("lockytest: power up")
      setup()
    }
  }

  public func peripheralManager(peripheral: CBPeripheralManager, central: CBCentral, didSubscribeToCharacteristic characteristic: CBCharacteristic) {
    subscribers.modify {
      var theCentral: CBCentral!

      if let index = subscribers.indexOf(UUID: central.identifier) {
        theCentral = subscribers[index]
      } else {
        theCentral = central
        subscribers.append(central)
        numberOfSubscribers.value = subscribers.count
      }

      if pendingEvents.keys.contains(theCentral.identifier) {
        let queue = pendingEvents[theCentral.identifier]!
        pendingEvents.removeValueForKey(theCentral.identifier)

        for event in queue {
          switch(event) {
          case .Response(let data):
            send(binaryData: data, characteristic: responseChannel, centrals: [theCentral])
            break
            //					case .Notificaiton(let data):
            //						send(binaryData: data, characteristic: notificationChannel, centrals: [theCentral])
            //						break
          }
        }
        NSLog("sent to subscriber (1)")
      }
    }
  }

  public func peripheralManager(peripheral: CBPeripheralManager, central: CBCentral, didUnsubscribeFromCharacteristic characteristic: CBCharacteristic) {
    subscribers.modify {
      if let index = subscribers.indexOf(UUID: central.identifier) {
        NSLog("subscriber is removed")
        subscribers.removeAtIndex(index)
        numberOfSubscribers.value = subscribers.count
      }
    }
  }

  public func peripheralManager(peripheral: CBPeripheralManager, didReceiveWriteRequests requests: [CBATTRequest]) {
    NSLog("write request.")

    for request in requests {
      guard request.characteristic.UUID == Lock.requestUUID else {
        manager.respondToRequest(request, withResult: .RequestNotSupported)
        continue
      }

      if let rawData = request.value {
        guard let packet = PacketConverter.requestPacket(deviceKeyPair: deviceKeyPair, data: rawData) else {
          NSLog("device: failed to retrieve the packet. (1)")
          manager.respondToRequest(request, withResult: .UnlikelyError)
          continue
        }

        guard let requestDict = packet.dictionary["auth-request"] as? NSDictionary else {
          NSLog("device: failed to receive JSON object. (2)")
          manager.respondToRequest(request, withResult: .UnlikelyError)
          continue
        }

        guard let passcode = requestDict["passcode"] as? String else {
          NSLog("device: invalid auth-request packet.")
          manager.respondToRequest(request, withResult: .UnlikelyError)
          continue
        }

        let username = requestDict.valueForKey("username") as? String

        let responsePacket: Packet

        switch (passcode == "32743274") {
        case true:
          responsePacket = Packet(clientPublicKey: packet.clientPublicKey,
                                  dictionary: ["auth-response": ["status": "success"]])

          self.response.value = .Success(username)
          if !authenticated.contains(request.central.identifier) {
            authenticatedCount.value++
            authenticated.insert(request.central.identifier)
            subscriberPublicKey[request.central.identifier] = packet.clientPublicKey
          }

          unlock()
          break
        case false:
          responsePacket = Packet(clientPublicKey: packet.clientPublicKey,
                                  dictionary: ["auth-response": ["status": "failed"]])

          self.response.value = .Failed
          break
        }

        guard let rawData = PacketConverter.dataFor(response: responsePacket, deviceKeyPair: deviceKeyPair) else {
          preconditionFailure("ERROR: cannot encrypt response packet")
        }

        NSLog("response with success.")
        manager.respondToRequest(request, withResult: .Success)

        if !subscribers.contains(UUID: request.central.identifier) {
          if !pendingEvents.keys.contains(request.central.identifier) {
            pendingEvents.updateValue([], forKey: request.central.identifier)
          }
          pendingEvents[request.central.identifier]!.append(.Response(rawData))
          NSLog("request queued")
        } else {
          try send(binaryData: rawData,
                   characteristic: responseChannel,
                   centrals: [request.central])
          NSLog("request pushed")
        }

        numberOfTransactions.value++
      }
    }
  }

  public func peripheralManagerDidStartAdvertising(peripheral: CBPeripheralManager, error: NSError?) {
    if let error = error {
      NSLog("advertising: [error] \(error.description)")
    } else {
      NSLog("start advertising")
      advertising.value = true
    }
  }

  public func peripheralManager(peripheral: CBPeripheralManager, didAddService service: CBService, error: NSError?) {
    if let error = error {
      NSLog("addservice: [error] \(error.description)")
    } else {
      NSLog("service is added.")
    }
  }

  private func send(binaryData data: NSData, characteristic: CBMutableCharacteristic, centrals: [CBCentral]) {
    sendQueue.addOperation(SendOperation(binaryData: data,
      characteristic: characteristic,
      centrals: centrals,
      batchingSize: centrals.reduce(0) { max($0, $1.maximumUpdateValueLength) },
      device: self))
  }


  public func peripheralManagerIsReadyToUpdateSubscribers(peripheral: CBPeripheralManager) {
    sendingOp?.send()
  }
}

private class SendOperation: NSOperation {
  private var batchingSize: Int
  private var buffer: NSData!
  private var iterator: Int
  private var characteristic: CBMutableCharacteristic
  private var centrals: [CBCentral]
  private let device: LockDevice

  private var _isFinished: Bool = false {
    willSet { willChangeValueForKey("isFinished") }
    didSet { didChangeValueForKey("isFinished") }
  }

  private var _isExecuting: Bool = false {
    willSet { willChangeValueForKey("isExecuting") }
    didSet { didChangeValueForKey("isExecuting") }
  }

  init(binaryData: NSData, characteristic: CBMutableCharacteristic, centrals: [CBCentral], batchingSize: Int, device: LockDevice) {
    self.buffer = binaryData
    self.characteristic = characteristic
    self.centrals = centrals
    self.batchingSize = batchingSize
    self.device = device
    iterator = 0
  }

  override func start() {
    device.sendingOp = self
    device.notifying.value = true
    send()
  }

  private func send() {
    _isExecuting = true

    while (iterator < buffer.length) {
      var range = NSMakeRange(iterator, batchingSize)

      if NSMaxRange(range) >= buffer.length {
        range = NSMakeRange(iterator, buffer.length - iterator)
      }

      if device.manager.updateValue(buffer.subdataWithRange(range),
                                    forCharacteristic: characteristic,
                                    onSubscribedCentrals: centrals) {
        iterator += batchingSize
        NSLog("sendData: \(iterator - 1) / \(buffer.length) sent")
      } else {
        _isExecuting = false
        return
      }
    }

    if iterator >= buffer.length {
      if device.manager.updateValue(NSData(),
                                    forCharacteristic: characteristic,
                                    onSubscribedCentrals: centrals) {
        NSLog("sendData: End of Data sent")
        _isExecuting = false
        _isFinished = true
        device.sendingOp = nil
        device.notifying.value = false
      }
    }
  }
}