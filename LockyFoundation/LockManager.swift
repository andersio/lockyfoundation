//
//  LockManager.swift
//  LockyFoundation
//
//  Copyright © 2015 Anders Ha. All rights reserved.
//  Licensed under Apache License v2.0.
//

import Foundation
import CoreBluetooth
import MantleData
import ReactiveCocoa

private let PromixityThersold = 3.0
private let ProximityOneMeterRSSI = -45.0
private let ProximitySensitivity = 2.0
private let ProximityPathLossExponent = 3.0  //2 -4

private let ScanningUpdateThresold = 3.0
private let UnadvertisingLockTimeout = 60.0  // For advertising locks
private let UnreachableLockTimeout = 10.0    // For persisted locks

private let NotificationMinimumInterval = 30.0
private let ProximityInfoTimeout = 30.0
private let OutOfRangeDetectionTimeGap = 30.0

private let BlockNotificationAfterBackgroundAction = 3600.0
private let BlockNotificationAfterEnteringBackground = 2.0

public enum AuthenticationResult {
  case Success
  case Failed
  case ConnectionFailed
}

public class LockManager: NSObject {
  private var recovered: Bool = false
  private var poweredOn: Bool = false
  private var scanning: Bool = false
  private var background: Bool = false

  private(set) public var centralManager: CBCentralManager!
  internal lazy var keychain = KeychainSwift(keyPrefix: "locky-")

  public let deviceKey = DeviceKeyPair()

  public let locks: AnyReactiveSet<Lock>

  internal let _advertisingLocks: ArraySetSection<Lock>
  internal let _persistedLocks: ArraySetSection<Lock>

  private(set) public var doorbellNotification = MutableProperty<(Lock, UIImage)?>(nil)
  private(set) public var newDeviceNotification = MutableProperty<Lock?>(nil)

  private var cleanupTimer: NSTimer?
  private var offlineDetectionTimer: NSTimer?
  private var globalBackgroundTimer: CFTimeInterval?

  public override init() {
    let locks = ArraySet<Lock>(sectionCount: 2)

    self.locks = AnyReactiveSet(locks)
    self._advertisingLocks = locks[1]
    self._persistedLocks = locks[0]

    super.init()

    centralManager = CBCentralManager(delegate: self,
                                      queue: dispatch_get_main_queue(),
                                      options: [CBCentralManagerOptionRestoreIdentifierKey: CentralUUID])

    offlineDetectionTimer = NSTimer.scheduledTimerWithTimeInterval(15.0,
                                                                   target: self,
                                                                   selector: "detectOfflineLocks:",
                                                                   userInfo: nil,
                                                                   repeats: true)

    NSNotificationCenter.defaultCenter().addObserver(self,
                                                     selector: "applicationDidEnterBackground",
                                                     name: UIApplicationDidEnterBackgroundNotification,
                                                     object: UIApplication.sharedApplication())

    NSNotificationCenter.defaultCenter().addObserver(self,
                                                     selector: "applicationWillEnterForeground",
                                                     name: UIApplicationWillEnterForegroundNotification,
                                                     object: UIApplication.sharedApplication())
  }

  public func applicationDidEnterBackground() {
    background = true
    globalBackgroundTimer = CFAbsoluteTimeGetCurrent() + BlockNotificationAfterEnteringBackground
  }

  public func applicationWillEnterForeground() {
    background = false
    _persistedLocks.modify {
      for lock in _persistedLocks {
        lock.blockNotificationUntilTime = nil
        lock.backgroundNotified = false
      }
    }
  }

  private func lockForPeripheral(peripheral: CBPeripheral) -> Lock? {
    var lock: Lock? = nil

    _persistedLocks.modify {
      if let index = _persistedLocks.indexOf(UUID: peripheral.identifier) {
        lock = _persistedLocks[index]
      }
    }

    if let lock = lock {
      return lock
    }

    _advertisingLocks.modify {
      if let index = _advertisingLocks.indexOf(UUID: peripheral.identifier) {
        lock = _advertisingLocks[index]
      }
    }

    return lock
  }

  public func lockForUUIDString(UUIDString: String) -> Lock? {
    var lock: Lock!

    _persistedLocks.modify {
      if let index = _persistedLocks.indexOf(UUID: NSUUID(UUIDString: UUIDString)!) {
        lock = _persistedLocks[index]
      }
    }

    if let lock = lock {
      return lock
    }

    _advertisingLocks.modify {
      if let index = _advertisingLocks.indexOf(UUID: NSUUID(UUIDString: UUIDString)!) {
        lock = _advertisingLocks[index]
      }
    }

    if let lock = lock {
      return lock
    }

    return nil
  }

  public func persistLock(lock: Lock) {
    _advertisingLocks.modify {
      if let index = _advertisingLocks.indexOf(lock) {
        _advertisingLocks.removeAtIndex(index)
      }
    }

    _persistedLocks.append(lock)
    lock.persisted = true

    let UUIDStringArray = _persistedLocks.map { $0.UUIDString }
    let NameStringArray = _persistedLocks.map { $0.name.value }

    NSUserDefaults.standardUserDefaults().setObject(UUIDStringArray, forKey: "LockyUUID")
    NSUserDefaults.standardUserDefaults().setObject(NameStringArray, forKey: "LockyName")

  }

  public func unpersistLock(lock: Lock) {
    _persistedLocks.modify {
      if let index = _persistedLocks.indexOf(lock) {
        _persistedLocks.removeAtIndex(index)
      }
    }
    _advertisingLocks.append(lock)
    lock.persisted = false

    let UUIDStringArray = _persistedLocks.map { $0.UUIDString }
    let NameStringArray = _persistedLocks.map { $0.name.value }

    NSUserDefaults.standardUserDefaults().setObject(UUIDStringArray, forKey: "LockyUUID")
    NSUserDefaults.standardUserDefaults().setObject(NameStringArray, forKey: "LockyName")
  }

  public func truncateLockLists() {
    _advertisingLocks.map { $0.peripheralObject }.forEach(centralManager.cancelPeripheralConnection)
    _persistedLocks.map { $0.peripheralObject }.forEach(centralManager.cancelPeripheralConnection)

    _advertisingLocks.removeAll(keepCapacity: false)
    _persistedLocks.removeAll(keepCapacity: false)
    keychain.clear()

    NSUserDefaults.standardUserDefaults().setObject([], forKey: "LockyUUID")
    NSUserDefaults.standardUserDefaults().setObject([], forKey: "LockyName")
  }

  public func authenticateLock(lock: Lock, username: String, callback: AuthenticationResult -> Void) {
    // CBManger cannot be used concurrently. So locking is mandatory.
    // But dispatches are started in a concurrent queue, so that the calling thread would not be blocked.
    guard let passcode = lock.keychainItem else {
      callback(.Failed)
      NSLog("No Corresponding Keychain Item Found.")
      return
    }

    lock.initiateRequest(
      LockRequest(key: "auth-request",
        data: ["passcode": passcode, "username": username],
      responseKey: "auth-response") { response in
        if self.background {
          lock.blockNotificationUntilTime = CFAbsoluteTimeGetCurrent() + BlockNotificationAfterBackgroundAction
        }

        switch(response) {
        case .Success(let dictionary):
          guard let authResponse = dictionary["status"] as? String else {
            callback(.ConnectionFailed)
            return
          }

          switch(authResponse) {
          case "success":
            callback(.Success)
            if !lock.persisted {
              self.persistLock(lock)
            }
            break
          case "failed": fallthrough
          default:
            callback(.Failed)
            if lock.persisted {
              self.unpersistLock(lock)
            }
            break
          }
          break
        case .Aborted:
          callback(.ConnectionFailed)
          break
        }
      })
  }

  public func startScanning() {
    scanning = true
    cleanupTimer = NSTimer.scheduledTimerWithTimeInterval(15.0, target: self, selector: "cleanUpNonAdvertisingLocks:", userInfo: nil, repeats: true)
  }

  public func stopScanning() {
    cleanupTimer?.invalidate()
    cleanupTimer = nil
    scanning = false
  }

  public func detectOfflineLocks(timer: NSTimer) {
    if _persistedLocks.count == 0 {
      return
    }

    let currentTime = CFAbsoluteTimeGetCurrent()
    _persistedLocks.modify {
      for lock in _persistedLocks {
        if !lock.outOfRange.value && currentTime - lock.updateTimer! >= UnreachableLockTimeout {
          lock.outOfRange.value = true
          NSLog("Lock is unreachable.")
        }
      }

    }
  }

  public func cleanUpNonAdvertisingLocks(timer: NSTimer) {
    let currentTime = CFAbsoluteTimeGetCurrent()
    _advertisingLocks.modify {
      var indices: [Int] = []
      for (i, lock) in _advertisingLocks.enumerate() {
        if currentTime - lock.updateTimer! >= UnadvertisingLockTimeout {
          indices.append(i)
        }
      }

      indices.reverse().forEach { _advertisingLocks.removeAtIndex($0) }
    }
  }

  internal func processDoorbellNotification(lock: Lock, image: UIImage) {
    doorbellNotification.value = (lock, image)
  }
}

extension LockManager: CBCentralManagerDelegate {
  public func centralManager(central: CBCentralManager, didDiscoverPeripheral peripheral: CBPeripheral, advertisementData: [String : AnyObject], RSSI: NSNumber) {
    let currentTime = CFAbsoluteTimeGetCurrent()
    var shouldReturn: Bool = false
    let distance = approximateDistance(RSSI)
    let outOfRange = distance > PromixityThersold

    _persistedLocks.modify {
      if let index = _persistedLocks.indexOf(UUID: peripheral.identifier) {
        let lock = _persistedLocks[index]

        switch background {
        case true:
          if !lock.outOfRange.value && outOfRange {
            // Condition: Not out of range before, and is out of range now.
            lock.outOfRange.value = true
            lock.backgroundNotified = false
          } else if !outOfRange && currentTime - lock.updateTimer! >= ScanningUpdateThresold {
            // Condition: Out of range before, and is within range now.
            if lock.outOfRange.value && currentTime >= lock.backgroundTimer && currentTime >= globalBackgroundTimer {
              lock.outOfRange.value = false
              lock.backgroundTimer = currentTime + OutOfRangeDetectionTimeGap

              if !lock.backgroundNotified {
                if !lock.isConnected {
                  lock.connect()
                }

                lock.backgroundNotified = true

                if currentTime > lock.blockNotificationUntilTime ?? 0 {
                  newDeviceNotification.value = lock
                }
              }
            }

            lock.updateTimer = currentTime
          }

        case false:
          if !lock.outOfRange.value && outOfRange && !lock.hasPendingAction {
            lock.hasPendingAction = true
            MainQueue.after(ScanningUpdateThresold) {
              if lock.outOfRange.value {
                lock.outOfRange.value = true
                lock.proximity.value = -1
              }

              lock.hasPendingAction = false
            }
          } else if !outOfRange && currentTime - lock.updateTimer! >= ScanningUpdateThresold {
            if lock.outOfRange.value {
              lock.outOfRange.value = false

              if !lock.isConnected {
                lock.connect()
              }
            }

            let name = advertisementData[CBAdvertisementDataLocalNameKey] as? String ?? "(locky-nil)"
            if name != lock.name.value {
              lock.name.value = name
            }

            lock.proximity.value = distance
            lock.updateTimer = currentTime
          }
        }


        shouldReturn = true
        return
      }
    }

    if shouldReturn {
      return
    }

    if scanning {
      _advertisingLocks.modify {
        if let index = _advertisingLocks.indexOf(UUID: peripheral.identifier) {
          let lock = _advertisingLocks[index]
          if currentTime - lock.updateTimer! >= ScanningUpdateThresold  {
            if !lock.outOfRange.value && !lock.processing && outOfRange && !lock.hasPendingAction {
              lock.hasPendingAction = true
              MainQueue.after(ScanningUpdateThresold) {
                if lock.outOfRange.value {
                  self._advertisingLocks.modify {
                    if let index = self._advertisingLocks.indexOf(UUID: peripheral.identifier) {
                      self._advertisingLocks.removeAtIndex(index)
                    }
                  }
                }

                lock.hasPendingAction = false
              }
            } else if !outOfRange {
              let name = advertisementData[CBAdvertisementDataLocalNameKey] as? String ?? "(locky-nil)"
              if name != lock.name.value {
                lock.name.value = name
              }

              lock.proximity.value = distance
              lock.updateTimer = currentTime
            }
          }
        } else if !outOfRange {
          let lock = Lock(lockManager: self, peripheral: peripheral)
          lock.outOfRange.value = false
          lock.name.value = advertisementData[CBAdvertisementDataLocalNameKey] as? String ?? "(locky-nil)"
          lock.proximity.value = distance
          _advertisingLocks.append(lock)

          lock.updateTimer = currentTime
        }
      }
    }
  }

  public func centralManagerDidUpdateState(central: CBCentralManager) {
    switch(central.state) {
    case .PoweredOn:
      poweredOn = true

      let defaults = NSUserDefaults.standardUserDefaults()

      if !recovered {
        if let UUIDStringArray = defaults.arrayForKey("LockyUUID") as? [String], NameStringArray = defaults.arrayForKey("LockyName") as? [String] where UUIDStringArray.count == NameStringArray.count {
          let time = CFAbsoluteTimeGetCurrent()
          let UUIDs = UUIDStringArray.map { NSUUID(UUIDString: $0)! }
          let peripherals = central.retrievePeripheralsWithIdentifiers(UUIDs)
          var locks = [Lock]()

          for (i, peripheral) in peripherals.enumerate() {
            let lock = Lock(lockManager: self, peripheral: peripheral, persisted: true, time: time)
            lock.name.value = NameStringArray[i]
            lock.updateTimer = 0
            lock.persisted = true
            locks.append(lock)
            
            if !lock.isConnected {
              lock.connect()
            }
          }
          
          _persistedLocks.appendContentsOf(locks)
        }
        recovered = true
      }
      
      central.scanForPeripheralsWithServices([Lock.ServiceUUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
      break
    default:
      break
    }
  }
  
  public func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
    lockForPeripheral(peripheral)?.didConnectPeripheral()
    NSLog("lockmanager: connected")
  }
  
  public func centralManager(central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: NSError?) {
    if let lock = lockForPeripheral(peripheral) {
      lock.didDisconnectPeripheral(error)
      if !lock.outOfRange.value {
        lock.outOfRange.value = true
      }
    }
    
    NSLog("lockmanager: disconnected")
  }
  
  public func centralManager(central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
    lockForPeripheral(peripheral)?.didFailToConnectPeripheral(error)
    NSLog("lockmanager: failed to connect")
  }
  
  public func centralManager(central: CBCentralManager, willRestoreState dict: [String : AnyObject]) {
    
  }
}

public func approximateDistance(RSSI: NSNumber) -> Double {
  let rawRSSI = -1 * min(RSSI.doubleValue, 0.0) + ProximityOneMeterRSSI
  return round(pow(10.0, rawRSSI / (10 * ProximityPathLossExponent)) * 1000) / 1000
}