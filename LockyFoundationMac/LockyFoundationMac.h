//
//  LockyFoundationMac.h
//  LockyFoundation
//
//  Copyright © 2015 Anders Ha. All rights reserved.
//  Licensed under Apache License v2.0.
//

#import <Cocoa/Cocoa.h>

//! Project version number for LockyFoundationMac.
FOUNDATION_EXPORT double LockyFoundationMacVersionNumber;

//! Project version string for LockyFoundationMac.
FOUNDATION_EXPORT const unsigned char LockyFoundationMacVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LockyFoundationMac/PublicHeader.h>


